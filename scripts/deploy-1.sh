#!/bin/bash

# Login into ECR
aws ecr get-login-password --region "$AWS_REGION" | docker login --username AWS --password-stdin "$AWS_ACCOUNT_ID".dkr.ecr."$AWS_REGION".amazonaws.com

# PATH TO docker-composer.yml file
DEPLOY_FOLDER=$DOCKER_PATH
echo "Deploy path: $DEPLOY_FOLDER"

if [ ! "$DEPLOY_FOLDER" ]; then
    echo "Deploy path isn't specified. Please add as argument"
    exit 1
fi

# DEPLOY on FOLDER
cd "$DEPLOY_FOLDER" || exit

# DOCKER PULL & UPDATE CONTAINERS
docker compose pull
# DOCKER UP
docker compose -p $DOCKER_STACK up $DOCKER_SERVICE -d
