#!/bin/bash
STACK=$DOCKER_STACK
SERVICE=$DOCKER_SERVICE

# PATH TO docker-composer.yml file
DEPLOY_FOLDER=$DOCKER_PATH
echo "Deploy path: $DEPLOY_FOLDER"

if [ ! "$STACK" ]; then
    echo "Stack isn't specified. Please add STACK as argument"
    exit 1
fi

if [ ! "$SERVICE" ]; then
    echo "Service isn't specified. Please add SERVICE as argument"
    exit 1
fi

if [ ! "$DEPLOY_FOLDER" ]; then
    echo "Deploy path isn't specified. Please add DEPLOY_FOLDER as argument"
    exit 1
fi

# ENTER TO FOLDER
cd "$DEPLOY_FOLDER" || exit

# Login into ECR
aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com

#PULL & UPDATE CONTAINERS
docker compose pull
docker compose -p $STACK up $SERVICE -d

