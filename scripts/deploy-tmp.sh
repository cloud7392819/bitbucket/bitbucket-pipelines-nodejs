#!/bin/bash

# Login into ECR
aws ecr get-login-password --region <region> | docker login --username AWS --password-stdin <id>.dkr.ecr.<region>.amazonaws.com

# PATH TO docker-composer.yml file
DEPLOY_FOLDER=$DOCKER_PATH
echo "Deploy path: $DEPLOY_FOLDER"

if [ ! "$DEPLOY_FOLDER" ]; then
    echo "Deploy path isn't specified. Please add as argument"
    exit 1
fi

# ENTER TO FOLDER
cd "$DEPLOY_FOLDER" || exit

#PULL & UPDATE CONTAINERS
docker compose pull
docker compose up <services-to-docker-compose> -d
