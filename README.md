# bitbucket-pipelines-nodejs

##################################################################
##  Bitbucket-pipelines-nodejs-1
##################################################################






##################################################################
##  Bitbucket-pipelines-nodejs-2
##################################################################

##################################################################
### Repository setting >> Repository variables
##################################################################

```text
AWS_ACCESS_KEY_ID: 123456789
AWS_SECRET_ACCESS_KEY: 123456789
AWS_REGION: <region>
IMAGE_NAME: <ecr_name> 
SSH_USER: <user_name>
PORT: <number_port>
HOST: <IP>
```

##################################################################
### Repository setting >> Deployments
##################################################################

##################################################################
#### Test environments [DEV]
#### Add environment
##################################################################

```text
Environment: DEV 
Variables: [ Name: TAGS, Value: dev ]

Environment: SERVER_DEV 
Variables: [ Name: DOCKER_PATH, Value: /opt/efs/<name>/dev ]
Variables: [ Name: DOCKER_STACK, Value: -p dev ]
Variables: [ Name: EXTRA_ARGS, Value: -J <user>@<ip> ]
Variables: [ Name: AWS_REGION, Value: <aws_region> ]
Variables: [ Name: AWS_ACCOUNT_ID, Value: <id> ]
```

##################################################################
#### Staging environments [DEMO]
#### Add environment
##################################################################

```text
Environment: DEMO
Variables: [ Name: TAGS, Value: demo ]

Environment: SERVER_DEMO
Variables: [ Name: DOCKER_PATH, Value: /opt/efs/<name>/demo ]
Variables: [ Name: EXTRA_ARGS, Value: -J <user>@<ip> ]
```

##################################################################
#### Production environments [PROD]
#### Add environment
##################################################################

```text
Environment: PROD
Variables: [ Name: TAGS, Value: prod ]

Environment: SERVER_PROD 
Variables: [ Name: DOCKER_PATH, Value: /opt/efs/<name>/prod ]
Variables: [ Name: EXTRA_ARGS, Value: -J <user>@<ip> ]
```

##################################################################
### ADD HOST 
### Repository setting -> SSH Keys
##################################################################

```text
Generate keys
Known hosts -> <IP> -> Fetch -> ADD HOST
```








##################################################################
##  Bitbucket-pipelines-cdn
##################################################################

##################################################################
### Repository setting >> Repository variables
##################################################################

```text
AWS_ACCESS_KEY_ID: 123456789
AWS_SECRET_ACCESS_KEY: 123456789
AWS_REGION: <region>
AWS_S3_BUCKET_NAME: <s3_bucket_name> 
```



